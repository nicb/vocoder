
sr=48000
ksmps=3
0dbfs=1

zakinit 1,100



	instr 1
ifile=p4
as,athrow	diskin2	ifile,1
zaw as,0
	endin


	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ichannel=p6

as butterbp asrl, ifreq, ibandwidth
aenvfoll follow	as,0.01
kenvfoll downsamp aenvfoll
     zkw kenvfoll, ichannel
	endin

	instr 1000 ; printing instrument
k0  zkr 2
k1  zkr 3
k2  zkr 4
k3  zkr 5
k4  zkr 6
k5  zkr 7
k6  zkr 8
k7  zkr 9
k8  zkr 10
k9  zkr 11
k10  zkr 12
k11  zkr 13
k12  zkr 14
k13  zkr 15
k14  zkr 16
k15  zkr 17
k16  zkr 18
k17  zkr 19
    ; ecc. ecc.
know times
fprintks "env.data","%.4f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n",know, k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15, k16, k17
	endin
