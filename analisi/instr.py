class Instr:
    def __init__(self,number,at,dur):
        self.num=number
        self.at=at
        self.dur=dur

    def to_csound(self):
        return "i%d\t%.8f\t%12.8f\t"%(self.num,self.at,self.dur)

class I1(Instr):
    def __init__(self,number,at,dur,nome):
        super().__init__(number,at,dur)
        self.nome=nome

    def to_csound(self):
        string=super().to_csound()
        string2="%s"%(self.nome)
        return string+string2

class I91(Instr):
    def __init__(self,number,at,dur,frequenza,banda,channel):
        super().__init__(number,at,dur)
        self.frequenza=frequenza
        self.banda=banda
        self.canale=channel

    def to_csound(self):
        string=super().to_csound()
        string2="%.4f\t%.5f\t%d"%(self.frequenza,self.banda,self.canale)
        return string+string2
