from instr import Instr
from instr import I1
from instr import I91

class Scrittore:
    def __init__(self,file):
        self.instr=[]
        self.freqs=[]
        self.bandwidth=[]
        self.setUp(file)

    def setUp(self,file):
        f=open(file,"r")
        for line in f:
            (at,dur,nome,amp,freq,num_flt)=line.split()
        self.instr.append(I1(1,float(at),float(dur),nome))
        self.calc_freq(float(freq),int(num_flt))
        for i in range(int(num_flt)):
            self.instr.append(I91(91,float(at),float(dur),self.freqs[i],self.bandwidth[i],i+2))
        self.instr.append(Instr(1000,float(at),float(dur)))

    def to_csound(self):
        return '\n'.join([i.to_csound() for i in self.instr])




    def calc_freq(self,freq,num_flt):
        partial=freq*2.0
        for i in range(num_flt):
            delta=partial-freq
            divisione=delta/num_flt
            new_f=freq
            for i in range(num_flt):
                self.freqs.append(new_f)
                self.bandwidth.append(divisione*2.0)
                new_f+=divisione
            partial=+partial
