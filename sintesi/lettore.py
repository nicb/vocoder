from inviluppo import Inviluppo
import pdb

class Lettore:
    def __init__(self ,file,type_envs):
        self.file= file
        self.inviluppi=[]
        self.create(type_envs)

    def create(self,type_envs):
        #
        # quanti sono gli inviluppi? Ci vuole un modo per parametrizzare la
        # quantita`
        num_inv = 18
        for n in range(num_inv):
            self.inviluppi.append(Inviluppo(n+2,type_envs))
        f=open(self.file,"r")
        for line in f:
            (time, cols) = line.split(None, 1)
            time = float(time)
            col_list = cols.split(None)
            for c in range(len(col_list)):
                self.inviluppi[c].add_coord(time, float(col_list[c]))

    def toString(self):
        return '\n'.join([inv.toString() for inv in self.inviluppi])

    def to_csound(self,size):
        return '\n'.join([inv.to_csound(size) for inv in self.inviluppi])
