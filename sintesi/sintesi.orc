sr=48000
ksmps=3
0dbfs=1
nchnls=1

zakinit 1000,1

	instr 1
iamp=ampdbfs(p4)
ifreq=p5
knh=10*p6
klh=1
as	gbuzz iamp, ifreq,knh,klh,1,100
zaw as,0


	endin

	instr 91
asrl zar 0
ifreq=p4
ibandwidth=p5
ifn_channel=p6
kndx line 0,p3,1
kenv tablei kndx,ifn_channel,1
as butterbp asrl,ifreq,ibandwidth
zaw as*kenv,ifn_channel
	endin

	instr 1000
acompare zar 0
a1 zar 2
a2 zar 3
a3 zar 4
a4 zar 5
a5 zar 6
a6 zar 7
a7 zar 8
a8 zar 9
a9 zar 10
a10 zar 11
a11 zar 12
a12 zar 13
a13 zar 14
a14 zar 15
a15 zar 16
a16 zar 17
a17 zar 18
a18 zar 19
asum =	a1+a2+a3+a4+a5+a6+a7+a8+a9+a10+a11+a12+a13+a14+a15+a16+a17+a18
aout balance asum, acompare
  out aout
	endin
